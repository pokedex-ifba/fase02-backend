package dev.cancio.model
import kotlinx.serialization.Serializable

@Serializable
data class Pokemon(
    val id: Int = 0,
    val name: String = "who that's pokemon",
    var like: LikeType = LikeType.Unlike
)

@Serializable
enum class LikeType(type: Int){
    Like(0),
    Unlike(1)
}