package dev.cancio.plugins

import dev.cancio.repository.PokemonRepository
import io.ktor.server.routing.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.request.*
import io.ktor.server.routing.*


fun Application.configureSerialization() {
    val repository = PokemonRepository()

    routing {
        get("/pokemon") {
            call.respond(repository.getAllPokemon())
        }
        get("pokemonLike") {
            call.respond(repository.getLikePokemon())
        }
        get("/pokemon/{id}") {
            val idParam = call.parameters["id"] ?: return@get call.respondText(
                "parametro id nao encontrado",
                status = HttpStatusCode.BadRequest)
            call.respond(repository.getPokemonDetail(idParam))
        }
        post("pokemonLike/{id}") {
            val idParam = call.parameters["id"] ?: return@post call.respondText(
                "parametro id nao encontrado",
                status = HttpStatusCode.BadRequest)
            call.respond(repository.updatePokemon(idParam))
        }
        get("/") {
            call.respondText("Hello World!")
        }
    }
}
